﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boost : MonoBehaviour
{
    public SOBoost soBoost;
    [NaughtyAttributes.Tag] public string playerTag = "Player";
	public float respawnTime = 20;
	BoxCollider bc;
	MeshRenderer mr;

	/// <summary>
	/// Se inicializa el boost
	/// </summary>
	public void Start() {
		bc = GetComponent<BoxCollider>();
		mr = GetComponent<MeshRenderer>();		
	}

	/// <summary>
	/// Si el boost es recogido por el jugador, se le asigna el boost al jugador, 
	/// este desaparece y se invoca el respawn del boost en un tiempo de respawn 
	/// </summary>
	private void OnTriggerEnter(Collider other)
    {
        print("aa");
        if (other.transform.parent.CompareTag(playerTag)){
			print(other.name);
			other.transform.parent.GetComponent<PlayerStats>().getBoost(soBoost);
            print("Boost "+name+" obtenido");
			bc.enabled = false;
			mr.enabled = false;
			Invoke(nameof(RespawnBoost), respawnTime);
            //StartCoroutine(RespawnBoost());
        }
    }

	/// <summary>
	/// Respawn del boost
	/// </summary>
	private void RespawnBoost() {
		bc.enabled = true;
		mr.enabled = true;
	}
}
