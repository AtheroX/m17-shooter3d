﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayArmas : MonoBehaviour{

    public PlayerShoot listaArmas;
    public GameObject guiArma;
    public Color colorbase;
    public Color colorselecionado;

    /// <summary>
    /// Recorre la lista de armas y crea Huds para enseñar el arma y su botón/número
    /// </summary>
    private void Awake()
    {
        for (int i = 0; i < listaArmas.armas.Count; i++)
        {
            GameObject item = Instantiate(guiArma , transform);
            item.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = listaArmas.armas[i].sprite;
            item.transform.GetChild(0).GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().SetText(""+ (i+1));
        }
    }  
    
    /// <summary>
    /// Pone todos los iconos en un color gris excepto el seleccionado que lo pone en un color naranja
    /// </summary>   
    public void UpdateArmas(int o)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).GetChild(0).GetComponent<Image>().color = colorbase;
        }

        transform.GetChild(o).GetChild(0).GetComponent<Image>().color = colorselecionado;
    }
}
