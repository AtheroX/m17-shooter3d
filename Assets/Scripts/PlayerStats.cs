﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : Stats{

    public float velocidadRecarga = 1;
    public int stamina;
    public int velocidadMovimiento;
    public Slider hpbar;

	public Camera cam;

	/// <summary>
	/// Hace el start de Stats y establece los estados de HPBar
	/// </summary>
	public void Start() {
		base.Start();
		cam = Camera.main; //Consume bastante pero sólo es una vez en el player
		hpbar.maxValue = 1;
		hpbar.wholeNumbers = false;
		hpbar.value = 1;
		velocidadMovimiento = 1;
	}

	/// <summary>
	/// Functión que llaman los boosts cuando los pillas
	/// </summary>
	public void getBoost(SOBoost soBoost){
        StartCoroutine(TiempoBoost(soBoost));
	}

	/// <summary>
	/// Al inicio te añade las estadísticas del boost y si la velocidad es mayor de uno significa que te la aumenta
	/// y eso aumenta tu fov. Tras X tiempo se revierten esos cambios.
	/// </summary>
	/// <param name="soBoost"></param>
    IEnumerator TiempoBoost(SOBoost soBoost) {
		Hit(-soBoost.hp);
        velocidadMovimiento += (soBoost.velocidadMovimiento - 1);
		if(soBoost.velocidadMovimiento > 1)
			StartCoroutine(FOV());
        velocidadRecarga += (soBoost.velocidadRecarga - 1);
        stamina += soBoost.stamina;
		//cam.fieldOfView = (1+(velocidadMovimiento / 2)) * 80;

        yield return new WaitForSeconds(soBoost.tiempoDeUso);

        velocidadMovimiento -= (soBoost.velocidadMovimiento - 1);
		if (soBoost.velocidadMovimiento > 1)
			StartCoroutine(DeFOV());
        velocidadRecarga -= (soBoost.velocidadRecarga - 1);
        stamina -= soBoost.stamina;
	}

	/// <summary>
	/// A lo largo de 2 segundos aumenta tu fov
	/// </summary>
	/// <returns></returns>
	IEnumerator FOV() {
		float v = cam.fieldOfView + 15;
		for (float t = 0; t < 1; t += Time.deltaTime / 2) {
			cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, v, t);
			yield return null;
		}
		yield return null;
	}

	/// <summary>
	/// A lo largo de 2 segundos reduce tu fov
	/// </summary>
	/// <returns></returns>
	IEnumerator DeFOV() {
		float v = cam.fieldOfView - 15;
		for (float t = 0; t < 1; t += Time.deltaTime / 2) {
			cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, v, t);
			yield return null;
		}
		yield return null;
	}

	/// <summary>
	/// Llama al Hit de su padre para que le reste la vida y actualiza la barra de vida
	/// </summary>
	/// <param name="dmg"></param>
	public void Hit(int dmg){
		print("Me entra " + dmg);
		base.Hit(dmg);
		hpbar.value = (actHp*1f) / (maxHP*1f);
	}

	/// <summary>
	/// Cosas que debe hacer al morir.
	/// Está en blanco sin llamar al padre para que seas inmortal
	/// </summary>
    protected virtual void Die() {

	}

	/// <summary>
	/// Cosas que debe hacer al respawnear
	/// </summary>
	protected virtual void RespawnObject() {
		base.RespawnObject();
		//Respawnear
	}
}
