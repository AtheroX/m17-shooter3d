﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Arma", menuName = "new Arma", order = 1)]
public class SOArma : ScriptableObject
{
    //cantidad de balas por disparo
    [Tooltip("balas por disparo")]public int numBalasDisparo;  
    //empuje del disparo
    public int momento;
    //daño de cada disparo
    public int daño;
    //cuantas balas tiene cada cargador
    public int balas;
    //tiempo de recarga
    public float tiempoRecarga;
    //tiempo de espera entre cada disparo 
    [Tooltip("tiempo entre cada disparo")] public float cadencia;
    //si el arma necesita dispersion o no 
    [Tooltip("si es escopeta o arma pesada")] public bool needDispersion;
    //efecto de agujero de la bala
    public GameObject efectoBala;
    //sprite del hud
    public Sprite sprite;
}
