﻿using UnityEngine;

[CreateAssetMenu(fileName = "new Boost", menuName = "new Boost", order = 1)]
public class SOBoost : ScriptableObject{

    public int hp;
    public float velocidadRecarga;
    public int stamina;
    public int velocidadMovimiento;
    public float tiempoDeUso = 15f;
}
