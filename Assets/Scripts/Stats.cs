﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Sirve de base para todos los objetos que tengan vida
/// </summary>
public class Stats : MonoBehaviour{

	[SerializeField] protected int maxHP = 100;
	protected int actHp;
	[SerializeField] protected int timeToRespawn = 1;

	/* Forma en la que controlamos el Rb es con un bool que si está activo significa que tiene y si no no.
	 * Si está activo se verá en el inspector un apartado para seleccionar el Rigidbody, si no pones nada 
	 * significa que el rigidbody está en el mismo objeto, así que lo pilla en el start */
	public bool haveRB = true;
	[NaughtyAttributes.ShowIf("haveRB")] public Rigidbody whereIsMyRB;

	public void Start() {
		actHp = maxHP;
		if(haveRB && whereIsMyRB == null) {
			whereIsMyRB = GetComponent<Rigidbody>();
		}
	}

	/// <summary>
	/// Resta la vida al objeto y si ha muerto activa la corrutina de respawn
	/// </summary>
	/// <param name="dmg"></param>
	public void Hit(int dmg) {
		actHp -= dmg;
		if (actHp <= 0) {
			Die();
			StartCoroutine(Respawn());
		}
	}

	/// <summary>
	/// Cosas que debe hacer al morir
	/// </summary>
	protected virtual void Die() {
		print("Dieparent");

	}

	/// <summary>
	/// Para el tiempo necesario y respawnea
	/// </summary>
	IEnumerator Respawn() {
		yield return new WaitForSeconds(timeToRespawn);
		RespawnObject();
		yield return null;
	}

	/// <summary>
	/// Reasigna la vida máxima
	/// </summary>
	protected virtual void RespawnObject() {
		actHp = maxHP;
	}
}
