﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TargetIA : MonoBehaviour{

    NavMeshAgent agent;
	TargetStats ts;

	public SOArma arma;
	public LayerMask ignoreDisparable;
	[NaughtyAttributes.Tag] public string playerTag = "Player";
	[NaughtyAttributes.Required] public GameObject player;
	private PlayerStats ps;
	public Vector3 offset;
	public float precision;

	[Range(1, 50)] public float radioDeMovimiento = 10;
	[Range(1, 50)] public float radioDeDisparo = 5;

	private bool canShoot = true;

	void Start(){
        agent = GetComponent<NavMeshAgent>();
        ts = GetComponent<TargetStats>();
        StartCoroutine(SearchForPlayer());
		ps = player.GetComponent<PlayerStats>();
    }

    /// <summary>
    /// Si estoy vivo y a distancia suficiente (Está hecho con el vector magnitud porque es más rápido que el 
    /// Vector3.distance de unity) lanzo un rayo, si choca contra el player (CompareTag es más rápido que 
    /// tag ==) va a por él, si estoy yendo a por él y estoy a menos de X metros, disparo.
    /// </summary>
    IEnumerator SearchForPlayer() {
        Vector3 dir = new Vector3(
            player.transform.position.x - transform.position.x,
            player.transform.position.y - transform.position.y,
            player.transform.position.z - transform.position.z);

        
        if (ts.alive()) {
			float dist = Mathf.Sqrt(Mathf.Pow(dir.x, 2) + Mathf.Pow(dir.y, 2) + Mathf.Pow(dir.z, 2));
			if (dist < radioDeMovimiento) { 
				Ray ray = new Ray(transform.position, player.transform.position);
				RaycastHit hit;
				Debug.DrawRay(transform.position, dir, Color.red);
				if (Physics.Raycast(ray, out hit, radioDeDisparo)) {
					if (hit.collider.CompareTag("Player") || dist < 5f) {
						agent.SetDestination(transform.position);
						Choot();
					} else {
						agent.SetDestination(player.transform.position);
					}
				} else {
					agent.SetDestination(player.transform.position);
				}
			} else {
				agent.SetDestination(transform.position);
			}
		} else {
			agent.SetDestination(transform.position);
		}
		yield return null;
		StartCoroutine(SearchForPlayer());
    }

	/// <summary>
	/// Hace la acción del disparo, es bastante similar al PlayerShoot pero con la peculiaridad de que no tiene cargador de balas
	/// así que tiene la cadencia metida a la fuerza con un Cool Cooldown 😎
	/// </summary>
	private void Choot() {
		if (!canShoot)
			return;
		canShoot = false;

		//Dispara la cantidad de balas
		for (int i = 0; i < arma.numBalasDisparo; i++) {
			Vector3 newForward = new Vector3(
					player.transform.position.x - transform.position.x + UnityEngine.Random.Range(-precision, precision*3),
					player.transform.position.y - transform.position.y + UnityEngine.Random.Range(-precision, precision),
					player.transform.position.z - transform.position.z + UnityEngine.Random.Range(-precision, precision*3))+offset;
			

			Ray ray = new Ray(transform.position, newForward);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, radioDeDisparo, ignoreDisparable)) {
				print(hit.collider.name);

				if (hit.transform.CompareTag(playerTag)) 
					ps.Hit(arma.daño);
				
				GameObject newExplosion = Instantiate(arma.efectoBala, hit.collider.transform);
				newExplosion.transform.position = hit.point;
				Destroy(newExplosion, 1f);
			}
			Debug.DrawLine(transform.position, hit.point, Color.green, 1f);
		}

		//Qué guapo hacer así los cooldowns, eh? 😎
		Invoke(nameof(ChootCooldown), arma.cadencia);
	}

	private void ChootCooldown() => canShoot = true;

	/// <summary>
	/// Dibuja las esferas de acción del enemigo
	/// </summary>
	public void OnDrawGizmosSelected() {
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(transform.position, radioDeMovimiento);
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, radioDeDisparo);
	}
}
