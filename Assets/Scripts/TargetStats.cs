﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetStats : Stats{

	Transform startPos;
	public AnimationClip ragDollRespawnAnim;
	Animator anim;

    /// <summary>
    /// Inicializa enemigo
    /// </summary>
	public void Start() {
		base.Start();
		anim = GetComponent<Animator>();
	}

    /// <summary>
    /// Muere, animación de morir, se desactiva el modelo base y se activa el ragdoll
    /// </summary>
	protected override void Die() {
        base.Die();
        anim.SetTrigger("Die");
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(true);
        //anim.enabled = true;
        //transform.GetChild(1).position = transform.GetChild(0).position;
        //base.whereIsMyRB.isKinematic = true;
        //GetComponent<Collider>().isTrigger = true;
        //startPos = transform.GetChild(0);
    }

    /// <summary>
    /// Respawnea en la posición dónde se encuentra, animación de respawn, se desactiva el ragdoll y se activa el modelo base
    /// </summary>
    protected override void RespawnObject() {
        base.RespawnObject();
        anim.SetTrigger("Respawn");
        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(false);
        //transform.position = startPos.position + Vector3.up;
        //base.whereIsMyRB.isKinematic = false;
        //GetComponent<Collider>().isTrigger = false;
        //anim.enabled = false;
    }

    /// <summary>
    /// Devuelve un bool dependiendo de si su vida está por encima de 0
    /// </summary>
    public bool alive() {
        return actHp > 0;
    }
}
