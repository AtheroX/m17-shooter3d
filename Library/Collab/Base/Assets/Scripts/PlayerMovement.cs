﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class PlayerMovement : MonoBehaviour {

	//Assingables
	[Required] [BoxGroup("Assignables")] public Transform playerCam;
	[Required] [BoxGroup("Assignables")] public Transform orientation;

	//Other
	private Rigidbody rb;

	//Rotation and look
	private float xRotation;
	private float sensitivity = 50f;
	[Tooltip("Sensivility option for menus of configuration")] private float sensMultiplier = 1f;

	//Movement
	[Foldout("Movement")] public float moveSpeed = 4500;
	[Foldout("Movement")] public float maxSpeed = 20;
	[Foldout("Movement")] public float runSpeed = 1.5f;
	[Foldout("Movement")] public bool running;
	[Foldout("Ground")] public bool grounded;
	[Foldout("Ground")] public LayerMask whatIsGround;

	[Foldout("Movement")] [Tooltip("A \"Friction\" that gets player on moving")] public float counterMovement = 0.175f;
	[Tooltip("For Joysticks, a threshold of activation for axis")] private float threshold = 0.01f;
	[Foldout("Slope")] public float maxSlopeAngle = 35f;

	//Crouch & Slide
	private Vector3 crouchScale = new Vector3(1, 0.5f, 1);
	private Vector3 playerScale;
	[Foldout("Slope")] [Tooltip("The force that player gets when starts to slide")] public float slideForce = 400;
	[Foldout("Slope")] [Tooltip("A friction in % for sliding")] public float slideCounterMovement = 0.2f;

	//Jumping
	private bool readyToJump = true;
	private float jumpCooldown = 0.25f;
	[Foldout("Movement")] public float jumpForce = 550f;

	//Input
	float x, y;
	bool jumping, sprinting, crouching;

	//Sliding
	private Vector3 normalVector = Vector3.up;
	private Vector3 wallNormalVector;
    
	void Awake() {
		rb = GetComponent<Rigidbody>();
	}

	void Start() {
		playerScale = transform.localScale;
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	private void FixedUpdate() {
		Movement();
	}

	private void Update() {
		GetInputs();
		Look();		
	}

	/// <summary>
	/// Gets all player's inputs
	/// </summary>
	private void GetInputs() {
		x = Input.GetAxisRaw("Horizontal");
		y = Input.GetAxisRaw("Vertical");
		jumping = Input.GetAxisRaw("Jump") == 1;

		//Crouching
		if (!crouching && Input.GetAxisRaw("Crouch") == 1) {
			crouching = true;
			StartCrouch();
		}

		if (crouching && Input.GetAxisRaw("Crouch") == 0) {
			crouching = false;
			StopCrouch();
		}

		//Running
		if (!running && Input.GetAxisRaw("Run") == 1) running = true;
		if (running && Input.GetAxisRaw("Run") == 0) running = false;

	}

	/// <summary>
	/// Rescales the player to his "chiquito" version
	/// Also if it is groudned and running, it gets a boost of velocity for sliding
	/// </summary>
	private void StartCrouch() {
		transform.localScale = crouchScale;
		transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);
		if (rb.velocity.magnitude > 0.5f) {
			if (grounded) {
				rb.AddForce(orientation.transform.forward * slideForce);
			}
		}
	}

	/// <summary>
	/// Rescales the player to his original size
	/// </summary>
	private void StopCrouch() {
		transform.localScale = playerScale;
		transform.position = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
	}

	private void Movement() {
		//Extra gravity
		rb.AddForce(Vector3.down * Time.deltaTime * 10);

		//Find actual velocity relative to where player is looking
		Vector2 mag = FindVelRelativeToLook();
		float xMag = mag.x, yMag = mag.y;

		//Counteract sliding and sloppy movement
		CounterMovement(mag);

		//If holding jump && ready to jump, then jump
		if (readyToJump && jumping) Jump();

		//Set max speed
		float maxSpeed = this.maxSpeed;

		//If sliding down a ramp, add force down so player stays grounded and also builds speed
		if (crouching && grounded && readyToJump) {
			rb.AddForce(Vector3.down * Time.deltaTime * 3000);
			//return;
		}

		//Stops sliding when not moving
		if (x > 0 && xMag > maxSpeed) x = 0;
		if (x < 0 && xMag < -maxSpeed) x = 0;
		if (y > 0 && yMag > maxSpeed) y = 0;
		if (y < 0 && yMag < -maxSpeed) y = 0;

		//Some multipliers
		float multiplier = 1f, multiplierV = 1f;

		// Movement in air
		if (!grounded) {
			multiplier = 0.5f;
			multiplierV = 0.5f;
		}

		// Movement while sliding
		if (grounded) {
            if (crouching) {

                multiplier = 0f;
                multiplierV = 0f;
            }else if (running) { 
				multiplier = runSpeed;
				multiplierV = runSpeed;
			}
		}
		//Apply forces to move player
		rb.AddForce(orientation.transform.forward * y * moveSpeed * Time.deltaTime * multiplier * multiplierV);
		rb.AddForce(orientation.transform.right * x * moveSpeed * Time.deltaTime * multiplier);
	}

	/// <summary>
	/// Jumps if isGrounded and doesnt have cooldown to jump
	/// </summary>
	private void Jump() {
		if (grounded && readyToJump) {
			readyToJump = false;

			//Add jump forces
			rb.AddForce(Vector2.up * jumpForce * 1.5f);
			rb.AddForce(normalVector * jumpForce * 0.5f);

			//Cool colddown 😎
			Invoke("ResetJump", jumpCooldown);
		}
	}

	private void ResetJump() {
		readyToJump = true;
	}

	private float desiredX;
	/// <summary>
	/// Rotates the camera by the Mouse/Joystick
	/// </summary>
	private void Look() {
		float mouseX = Input.GetAxis("Mouse X") * sensitivity * Time.fixedDeltaTime * sensMultiplier;
		float mouseY = Input.GetAxis("Mouse Y") * sensitivity * Time.fixedDeltaTime * sensMultiplier;

		//Find current look rotation
		Vector3 rot = playerCam.transform.localRotation.eulerAngles;
		desiredX = rot.y + mouseX;

		//Rotate and also make sure we dont over-under-rotate the camera
		xRotation -= mouseY;
		xRotation = Mathf.Clamp(xRotation, -90f, 70f);

		//Perform the rotations
		playerCam.transform.localRotation = Quaternion.Euler(xRotation, desiredX, 0);
		orientation.transform.localRotation = Quaternion.Euler(0, desiredX, 0);
	}

	/// <summary>
	/// Makes a friction that contraposes to the player's movement
	/// </summary>
	/// <param name="mag"></param>
	private void CounterMovement(Vector2 mag) {
		if (!grounded || jumping) return;

		//Slow down sliding
		if (crouching) {
			rb.AddForce(moveSpeed * Time.deltaTime * -rb.velocity.normalized * slideCounterMovement);
			return;
		}

		//Counter movement
		if (Mathf.Abs(mag.x) > threshold && Mathf.Abs(x) < 0.05f || (mag.x < -threshold && x > 0) || (mag.x > threshold && x < 0)) {
			rb.AddForce(moveSpeed * orientation.transform.right * Time.deltaTime * -mag.x * counterMovement);
		}
		if (Mathf.Abs(mag.y) > threshold && Mathf.Abs(y) < 0.05f || (mag.y < -threshold && y > 0) || (mag.y > threshold && y < 0)) {
			rb.AddForce(moveSpeed * orientation.transform.forward * Time.deltaTime * -mag.y * counterMovement);
		}

		//Limit diagonal running. 
		//Also causes a full stop if sliding fast and un-crouching.
		if (Mathf.Sqrt((Mathf.Pow(rb.velocity.x, 2) + Mathf.Pow(rb.velocity.z, 2))) > maxSpeed) {
			float fallspeed = rb.velocity.y;
			Vector3 n = rb.velocity.normalized * maxSpeed;
			rb.velocity = new Vector3(n.x, fallspeed, n.z);
		}
	}

	/// <summary>
	/// Find the velocity relative to where the player is looking
	/// Useful for vectors calculations regarding movement and limiting movement
	/// </summary>
	/// <returns></returns>
	public Vector2 FindVelRelativeToLook() {
		float lookAngle = orientation.transform.eulerAngles.y;
		float moveAngle = Mathf.Atan2(rb.velocity.x, rb.velocity.z) * Mathf.Rad2Deg;

		float u = Mathf.DeltaAngle(lookAngle, moveAngle);
		float v = 90 - u;

		float magnitue = rb.velocity.magnitude;
		float yMag = magnitue * Mathf.Cos(u * Mathf.Deg2Rad);
		float xMag = magnitue * Mathf.Cos(v * Mathf.Deg2Rad);

		return new Vector2(xMag, yMag);
	}

	/// <summary>
	/// Returns if the Floor has a lower angle than maxSlopeAngle variable
	/// </summary>
	/// <param name="v">Normal vector of the floor</param>
	/// <returns></returns>
	private bool IsFloor(Vector3 v) {
		float angle = Vector3.Angle(Vector3.up, v);
		return angle < maxSlopeAngle;
	}

	private bool cancellingGrounded;

	/// <summary>
	/// Handle ground detection
	/// </summary>
	private void OnCollisionStay(Collision other) {
		//Make sure we are only checking for walkable layers
		int layer = other.gameObject.layer;
		/*		
		 *		Explanation of this IF
		 * ---------------------------------
		 * 
		 * *<<* bitwise moves the 1 to the left as many times as layer position because each bit represents a layer
		 * *|* it's a logical bitwise OR, this way makes a merge between whatIsGround 
		 * Example of Layer of collision object:	00000000000000000000000001000000
		 * Example of whatIsGround Layer:			00000000000000000000000001000000
		 * OR operator =							00000000000000000000000001000000
		 * So it keeps the same, that means the Layer of collision is a floor
		 */
		if (whatIsGround != (whatIsGround | (1 << layer))) return; 

		//Iterate through every collision in a physics update
		for (int i = 0; i < other.contactCount; i++) {
			Vector3 normal = other.contacts[i].normal;
			//FLOOR
			if (IsFloor(normal)) {
				grounded = true;
				cancellingGrounded = false;
				normalVector = normal;
				CancelInvoke("StopGrounded");
			}
		}

		//Invoke ground/wall cancel, bcs cannot check normals with CollisionExit
		float delay = 3f;
		if (!cancellingGrounded) {
			cancellingGrounded = true;
			Invoke("StopGrounded", Time.deltaTime * delay);
		}
	}

	private void StopGrounded() {
		grounded = false;
	}
}
