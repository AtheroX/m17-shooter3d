﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : Stats{

    public float velocidadRecarga = 1;
    public int stamina;
    public int velocidadMovimiento;
    public Slider hpbar;

	public void Start() {
		base.Start();
		hpbar.maxValue = 1;
		hpbar.wholeNumbers = false;
		hpbar.value = 1;
	}

	public void getBoost(SOBoost soBoost){
        StartCoroutine(TiempoBoost(soBoost));
	}

    IEnumerator TiempoBoost(SOBoost soBoost) {
        actHp += soBoost.hp;
        velocidadMovimiento += (soBoost.velocidadMovimiento - 1);
        velocidadRecarga += (soBoost.velocidadRecarga - 1);
        stamina += soBoost.stamina;

        yield return new WaitForSeconds(soBoost.tiempoDeUso);

        actHp -= soBoost.hp;
        velocidadMovimiento -= (soBoost.velocidadMovimiento - 1);
        velocidadRecarga -= (soBoost.velocidadRecarga - 1);
        stamina -= soBoost.stamina;

    }

	public void Hit(int dmg){
		print("Me entra " + dmg);
		base.Hit(dmg);
		hpbar.value = (actHp*1f) / (maxHP*1f);
	}

    protected virtual void Die() {
		//base.Die();
		//Activa RagDoll
	}

	protected virtual void RespawnObject() {
		base.RespawnObject();
		//Respawnear
	}
}
