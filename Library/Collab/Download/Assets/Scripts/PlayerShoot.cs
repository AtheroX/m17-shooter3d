﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using TMPro;


public class PlayerShoot : MonoBehaviour {
    //camara
	[Required] public Camera mainCamera;

    //lista de armas y variables para el cambio de arma
    [ReorderableList] public List<SOArma> armas = new List<SOArma>();
    public int armaActual = 1;
    public int ArmaActual { get { return armaActual; } set { armaActual = value; armaACambiar = armas[ArmaActual]; CambiarArma(); }  }
    
    //cariables del arama y control de disparo y recarga
	[Expandable] public SOArma arma;
	[InputAxis] public string shootButton = "Fire1";
    [InputAxis] public string reloadButton = "Reload";
	private bool mantenerClick = false;

    //variables auxiliares de cantidad de balas , limitacion de distancia del raycast, precision y bools de comprobación de recarga
    private int auxCantBalas;
	private float limiteRaycast = 100f;
    private float precision = 0.05f;
	private bool cargada = true, recargando = false, canChangeWeapon = true;
	private Coroutine lastReload;

    //variables de comprobacion para indicar en que puede actuar el disparo o no 
    [NaughtyAttributes.Tag] public string disparableTags = "Disparable";
	public LayerMask ignorePlayer;

    //hud de armas y texto 
	[Required] public TextMeshProUGUI balasActuales;
	[Required] public TextMeshProUGUI balasTotales;
	[Required] public DisplayArmas displayArmas;


    private PlayerStats ps;
    private Vector3 fuerzaEmpuje;

	[Space][Space]
    [HorizontalLine(color: EColor.Red)] public SOArma armaACambiar;
    //boton en el inspector para cambiar arma
    [Button("Cambar arma")]
    private void CambiarArma() {
        ChangeWeapon(armaACambiar);
        armaACambiar = null;
    }

    private void Start() {
        ps = GetComponent<PlayerStats>();
        auxCantBalas = arma.balas;
        balasTotales.text = auxCantBalas + "";
        balasActuales.text = auxCantBalas + "";
        armaACambiar = armas[armaActual];
        CambiarArma();

	}

	void Update() {
        //input de disparar , empieza la corrutina de tiempo entre disparo que contiene la funcion de shoot , si te quedas con 0 balas recargas
		if (Input.GetButton(shootButton) && cargada && !recargando) {			
			StartCoroutine(tiempoEntreDisparo());

			if (auxCantBalas <= 0) {
				lastReload = StartCoroutine(Recarga());
			}
		}
        //input de recargar, puedes recargar si necesidad de gastar las balas
		if (Input.GetButtonDown(reloadButton)) {
			lastReload = StartCoroutine(Recarga());
		}

        //cambio de arma con la ruleta del raton
		if (canChangeWeapon) {
			float mousewheel = Input.GetAxisRaw("Mouse ScrollWheel");
			if (mousewheel > 0) {
				ArmaActual = armaActual - 1 < 0 ? armas.Count - 1 : armaActual - 1;
				CoolCooldownChangeWeapon();
			} else if (mousewheel < 0) {
				ArmaActual = armaActual + 1 > armas.Count - 1 ? 0 : armaActual + 1;
				CoolCooldownChangeWeapon();
			}
		}
        //cambio de arma con los numeros del teclado
        for (int i = 1; i <= armas.Count; i++)
        {
            if(Input.GetKeyDown(i + ""))
            {
                ArmaActual = i - 1;
            }
        }

	}


    /// <summary>
    /// corrutina de recarga, primero se comprueba si esta recargando , si no es asi se ponen las balas a 0, 
    /// espera un tiempo y recarga bala a bala cuando ya tiene todas las balas se espera otro pequeño tiempo y ya puedes disparar
    /// </summary>  
    IEnumerator Recarga()
    {
		if (recargando)
			yield return null;
		else {
			cargada = false;
			recargando = true;
            auxCantBalas = 0;
			yield return new WaitForSeconds(arma.cadencia / 2f);

			for (int i = 0; i < arma.balas; i++) {

				for (float t = 0; t < 1; t += Time.deltaTime / (arma.tiempoRecarga / ps.velocidadRecarga / arma.balas)) {
					yield return null;
				}
                auxCantBalas += 1;
				balasActuales.text = auxCantBalas + "";
			}
            //yield return new WaitForSeconds((arma.tiempoRecarga / ps.velocidadRecarga) - arma.cadencia);
            auxCantBalas = arma.balas;
			balasActuales.text = auxCantBalas + "";
			print("--------arma recargada---------");
			yield return new WaitForSeconds(arma.cadencia / 2f);

			recargando = false;
			cargada = true;
		}
    }
    /// <summary>
    /// corrutina para que se aplique el tiempo entre bala y bala para que no dispares todo el cargador de golpe
    /// </summary>
    IEnumerator tiempoEntreDisparo() {
		if (!mantenerClick) {
			mantenerClick = true;
			shoot();
			yield return new WaitForSeconds(arma.cadencia);
			mantenerClick = false;
		}
	}
    

    /// <summary>
    /// funcion de disparar
    /// </summary>
    void shoot() {
        //resta balas,lo actualiza en el texto y cuando se queda sin balas indica que esta descargada
        auxCantBalas--;
        balasActuales.text = auxCantBalas + "";
        if (auxCantBalas <= 0)
            cargada = false;

        //Por cada disparo mira si necesita dispersion , si no es necesario no la aplica, crea el raycast, calcula el momento en funcion de la distancia en la que impacta el ray 
        //Si colisiona con el tag de cosas que se pueden disparar , aplica el momento anteriormente calculado por el momento del arma en la direccion del impacto
        //Si el objetibo tiene estadisticas aplica a parte del empuje el daño, si no busca si tiene rigidbody y lo empuja
        //Luego crea el efecto del disparo donde colisiona y el sonido , al cabo de un tiempo lo elimina
		for (int i = 0; i < arma.numBalasDisparo; i++) {
			Vector3 newForward = mainCamera.transform.forward ;
			if (arma.needDispersion) 
				newForward = new Vector3(mainCamera.transform.forward.x + Random.Range(-precision, precision), 
                                         mainCamera.transform.forward.y + Random.Range(-precision, precision),
                                         mainCamera.transform.forward.z + Random.Range(-precision, precision));
			
			Ray ray = new Ray(mainCamera.transform.position, newForward);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, limiteRaycast, ignorePlayer)) {
				print(hit.collider.name);

                float porcentageMomento = Mathf.Max(limiteRaycast / hit.distance,8f);
				//print("---------------------" + porcentageMomento + "----------------------");
                if (hit.transform.CompareTag(disparableTags)) {

                    Vector3 fuerzaEmpuje = -hit.normal * arma.momento * porcentageMomento;

                    if (hit.collider.GetComponent<Stats>()) {
						Stats hitS = hit.collider.GetComponent<Stats>();

						if (hitS.haveRB) {

							hitS.whereIsMyRB.AddForceAtPosition(fuerzaEmpuje, hit.point);
						}
						hitS.Hit(arma.daño);
					} else if (hit.collider.GetComponent<Rigidbody>()) {

						hit.collider.GetComponent<Rigidbody>().AddForceAtPosition(fuerzaEmpuje, hit.point);
					}
				}
				GameObject newExplosion = Instantiate(arma.efectoBala, hit.collider.transform);
				newExplosion.transform.position = hit.point;
				Destroy(newExplosion, 1f);
			}
			Debug.DrawLine(mainCamera.transform.position, hit.point, Color.green, 1f);
		}
	}

    /// <summary>
    /// cambio de arma como bien dice el nombre de la funcion cambia el arma, tambien hace el cambio de color en el hud y el numero de balas , 
    /// cancela la corrutina de recarga y al final le hace un coolCooldown para que tengas que esperar un tiempo en recargar
    /// </summary>
	public void ChangeWeapon(SOArma arma) { 
		if (!canChangeWeapon)
			return;

		this.arma = arma;
		if(lastReload!=null) StopCoroutine(lastReload);
        auxCantBalas = arma.balas;
        balasTotales.text = auxCantBalas + "";
        balasActuales.text = auxCantBalas + "";
        displayArmas.UpdateArmas(ArmaActual);
		recargando = false;
		cargada = true;
		CoolCooldownChangeWeapon();
    }
    /// <summary>
    /// el mejor cooldown que se ha hecho en programacion
    /// </summary>
	public void CoolCooldownChangeWeapon() {
		canChangeWeapon = false;
		Invoke(nameof(AbleChangeWeapon), 0.25f);
	}

	public void AbleChangeWeapon() => canChangeWeapon = true;
}
